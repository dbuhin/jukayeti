﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameSession : MonoBehaviour
{
    [Header("Menu UI Elements")]
    [SerializeField] GameObject playButton;
    [SerializeField] GameObject quitButton;
    [SerializeField] GameObject record;

    [Header("Level UI Elements")]
    [SerializeField] GameObject attackButton;
    [SerializeField] GameObject slideButton;
    [SerializeField] GameObject lives;
    [SerializeField] GameObject timer;

    [Header("Level Game Objects")]
    [SerializeField] GameObject groundCenter;
    [SerializeField] GameObject groundRight;
    [SerializeField] GameObject snowCenter;
    [SerializeField] GameObject snowRight;
    [SerializeField] GameObject enemySpawner;
    [SerializeField] GameObject levelData;

    [Header("Game Elements")]
    [SerializeField] GameObject player;

    private Animator animator;
    private float time = 0f;
    private bool isPlaying = false;



    // Start is called before the first frame update
    void Start()
    {
        animator = player.GetComponent<Animator>();
        prepareGameToStart();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            time += Time.deltaTime;
            timer.GetComponent<TextMeshProUGUI>().text = time.ToString("F3");
        }

    }

    private void prepareGameToStart()
    {
        HideLevelUIElemnts();
        ShowMenuUIElemnts();
        LoadTimeRecord();
        StopMovingObjects();
        enemySpawner.SetActive(false);
        animator.SetBool("isMoving", false);
        animator.SetBool("isIdle", true);
        //levelData.SetActive(false);
        timer.GetComponent<TextMeshProUGUI>().text = "0:00";
    }

    public void StartGame()
    {
        HideMenuUIElemnts();
        ShowLevelUIElemnts();
        StartMovingObjects();
        enemySpawner.SetActive(true);
        levelData.SetActive(true);
        animator.SetBool("isMoving", true);
        animator.SetBool("isIdle", false);
    }

    private void StopMovingObjects()
    {
        groundCenter.GetComponent<GroundScroller>().setSpeed(0f);
        groundRight.GetComponent<GroundScroller>().setSpeed(0f);
        snowCenter.GetComponent<GroundScroller>().setSpeed(0f);
        snowRight.GetComponent<GroundScroller>().setSpeed(0f);
    }

    private void StartMovingObjects()
    {
        groundCenter.GetComponent<GroundScroller>().setSpeed(groundCenter.GetComponent<GroundScroller>().defaultSpeed);
        groundRight.GetComponent<GroundScroller>().setSpeed(groundRight.GetComponent<GroundScroller>().defaultSpeed);
        snowCenter.GetComponent<GroundScroller>().setSpeed(snowCenter.GetComponent<GroundScroller>().defaultSpeed);
        snowRight.GetComponent<GroundScroller>().setSpeed(snowRight.GetComponent<GroundScroller>().defaultSpeed);
    }

    private void LoadTimeRecord()
    {
        string path = Application.persistentDataPath + "map.dot";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            String loadData = formatter.Deserialize(stream) as String;
            record.GetComponent<TextMeshProUGUI>().text = loadData;
        }
        else
        {
            record.GetComponent<TextMeshProUGUI>().text = "0:00:000";
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void HideMenuUIElemnts()
    {
        HidePlayButton();
        HideQuitButton();
        HideRecord();
    }

    private void HideLevelUIElemnts()
    {
        HideAttackButton();
        HideSlideButton();
        HideLives();
        HideTimer();
    }

    private void ShowMenuUIElemnts()
    {
        ShowPlayButton();
        ShowQuitButton();
        ShowRecord();
    }

    private void ShowLevelUIElemnts()
    {
        ShowAttackButton();
        ShowSlideButton();
        ShowLives();
        ShowTimer();
    }

    private void HidePlayButton()
    {
        playButton.SetActive(false);
    }

    private void HideRecord()
    {
        record.gameObject.SetActive(false);
    }

    private void HideQuitButton()
    {
        quitButton.SetActive(false);
    }

    private void HideAttackButton()
    {
        attackButton.SetActive(false);
    }

    private void HideSlideButton()
    {
        slideButton.SetActive(false);
    }

    private void HideLives()
    {
        lives.SetActive(false);
    }

    private void HideTimer()
    {
        timer.SetActive(false);
    }

    private void ShowPlayButton()
    {
        playButton.SetActive(true);
    }

    private void ShowRecord()
    {
        record.gameObject.SetActive(true);
    }

    private void ShowQuitButton()
    {
        quitButton.SetActive(true);
    }

    private void ShowAttackButton()
    {
        attackButton.SetActive(true);
    }

    private void ShowSlideButton()
    {
        slideButton.SetActive(true);
    }

    private void ShowLives()
    {
        lives.SetActive(true);
    }

    private void ShowTimer()
    {
        timer.SetActive(true);
    }

    public void SetIsPlaying()
    {
        isPlaying = true;
    }
}