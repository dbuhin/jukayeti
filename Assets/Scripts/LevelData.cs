﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
    [SerializeField] public int speedUpIntervalTime = 25;
    [SerializeField] int speedUpIntervalLimit = 10;
    [SerializeField] [Range(0f, 2f)] public float speedupFactor = 1.1f;

    public int speedUpInterval = 0;

    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StartSpeedUp();
    }

    private void StartSpeedUp()
    {
        if (speedUpInterval <= speedUpIntervalLimit)
        {
            timer += Time.deltaTime;
            if (timer >= speedUpIntervalTime)
            {
                speedUpInterval++;
                timer = 0f;
            }
        }
        
    }
}
