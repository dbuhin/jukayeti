﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collidedObject)
    {
        if(collidedObject.tag!="Unbreakable")
        Destroy(collidedObject.gameObject);
        Debug.Log("Attack Point hit something!");
    }
}
