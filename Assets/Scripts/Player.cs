﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject attackPoint;

    public Animator animator;
    LevelData levelData;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        levelData = FindObjectOfType<LevelData>();
        attackPoint.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        animator.speed = Mathf.Pow(levelData.speedupFactor, levelData.speedUpInterval);
    }

    public void SetPlayerToMove()
    {
        animator.SetBool("isMoving", true);
        animator.SetBool("isIdle", false);
    }

    public void SetPlayerToIdle()
    {
        animator.SetBool("isMoving", false);
        animator.SetBool("isIdle", true);
    }

    public void Attack()
    {
        animator.SetTrigger("attack");
    }

    public void Slide()
    {
        animator.SetTrigger("slide");
    }

    private void EnableAttackCollider()
    {
        attackPoint.SetActive(true);
    }

    private void DisableAttackCollider()
    {
        attackPoint.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Juka got hit!");
    }
}
