﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] float minNextEnemyTime = 2f;
    [SerializeField] float maxNextEnemyTime = 3f;
    [SerializeField] GameObject[] enemy;

    bool isNextEnemyReady = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnNextEnemy();
    }

    private void SpawnNextEnemy()
    {
        if(isNextEnemyReady)
        {
            StartCoroutine(PrepareNextEnemy());
            var singleEnemy = Instantiate(enemy[UnityEngine.Random.Range(0, enemy.Length)], transform.position, Quaternion.identity) as GameObject;
            singleEnemy.transform.parent = gameObject.transform;
        }
    }

    IEnumerator PrepareNextEnemy()
    {
        isNextEnemyReady = false;
        yield return new WaitForSeconds(UnityEngine.Random.Range(minNextEnemyTime, maxNextEnemyTime));
        isNextEnemyReady = true;
    }
}
