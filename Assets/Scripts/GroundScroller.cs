﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScroller : MonoBehaviour
{
    [SerializeField] [Range(0f, 5f)] float speed = 0.5f;
    [SerializeField] public float defaultSpeed;

    LevelData levelData;

    // Start is called before the first frame update
    void Start()
    {
        levelData = FindObjectOfType<LevelData>();
    }

    void Update()
    {
        if(transform.position.x <= -19.18f)
        {
            transform.position = new Vector3(19.18f, transform.position.y, transform.position.z);
        }
        transform.Translate(Vector3.left * speed * Mathf.Pow(levelData.speedupFactor, levelData.speedUpInterval) * Time.deltaTime);
    }

    public void setSpeed (float newSpeed)
    {
        speed = newSpeed;
    }
}
