﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] [Range(0f, 5f)] float speed = 0.5f;

    LevelData levelData;

    // Start is called before the first frame update
    void Start()
    {
        levelData = FindObjectOfType<LevelData>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * speed * Mathf.Pow(levelData.speedupFactor, levelData.speedUpInterval) * Time.deltaTime);
    }
}
