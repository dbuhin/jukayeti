﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{

    [SerializeField][Range (0f, 1f)] float scrollSpeed = 0.08f;

    LevelData levelData;
    Material myMaterial;
    Vector2 offSet;

    bool isNextSpeedupIntervalReady = true;
    
    // Start is called before the first frame update
    void Start()
    {
        levelData = FindObjectOfType<LevelData>();
        myMaterial = GetComponent<Renderer>().material;
        offSet = new Vector2(scrollSpeed, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        myMaterial.mainTextureOffset += offSet * Time.deltaTime;
        if(isNextSpeedupIntervalReady)
        StartCoroutine(SpeedupInterval());
    }

    IEnumerator SpeedupInterval()
    {
        isNextSpeedupIntervalReady = false;
        yield return new WaitForSeconds(levelData.speedUpIntervalTime);
        CalculateNewOffset();
        isNextSpeedupIntervalReady = true;
    }

    private void CalculateNewOffset()
    {
        offSet.x = offSet.x * Mathf.Pow(levelData.speedupFactor, levelData.speedUpInterval);
    }
}
